$(document).ready(function() {
	$("#summernote").summernote({
		height: "100px",
		callbacks: {
			onImageUpload: function(image) {
				uploadImage(image[0]);
			},
			onMediaDelete: function(target) {
				deleteImage(target[0].src);
			}
		}
	});

	function uploadImage(image) {
		var data = new FormData();
		data.append("image", image);
		$.ajax({
			url: "<?= base_url('user/module/upload_image')?>",
			cache: false,
			contentType: false,
			processData: false,
			data: data,
			type: "POST",
			success: function(url) {
				$("#summernote").summernote("insertImage", url);
			},
			error: function(data) {
				console.log(data);
			}
		});
	}
});




$(document).ready(function() {
	// Bind normal buttons
	$(".ladda-button").ladda("bind", { timeout: 2000 });

	// Bind progress buttons and simulate loading progress
	Ladda.bind(".progress-demo .ladda-button", {
		callback: function(instance) {
			var progress = 0;
			var interval = setInterval(function() {
				progress = Math.min(progress + Math.random() * 0.1, 1);
				instance.setProgress(progress);

				if (progress === 1) {
					instance.stop();
					clearInterval(interval);
				}
			}, 200);
		}
	});

	var l = $(".ladda-button-demo").ladda();

	l.click(function() {
		// Start loading
		l.ladda("start");

		// Timeout example
		// Do something in backend and then stop ladda
		setTimeout(function() {
			l.ladda("stop");
		}, 12000);
	});
});

//function ajax add user
$(function() {
	$(".tampilAdd").on("click", function() {
		$("#modalLable").html("Add User");
		$(".modal-footer  button[type=submit]").html("Add User");
		$("#name").val("");
		$("#email").val("");
		$("#password").val("");
		$("#role_id").val("");
		$("#user_id").val("");
	});

	$(".tampilUbah").on("click", function() {
		$("#modalLable").html("Edit User");
		$(".modal-footer  button[type=submit]").html("Edit User");
		$(".custom-modal-text form").attr("action", "user/update");

		const id = $(this).data("id");

		$.ajax({
			url: "user/edit",
			data: { id: id },
			method: "POST",
			dataType: "json",
			success: function(data) {
				$("#name").val(data.name);
				$("#email").val(data.user_email);
				$("#password").val(data.user_password);
				$("#role_id").val(data.role_id);
				$("#user_id").val(data.user_id);
			}
		});
	});



	
});

	//addmenu ajax
	$(document).ready(function() {
		$(".menuAdd").on("click", function() {
			$("#modalMenu").html("Add Menu");
			$(".modal-footer  button[type=submit]").html("Add Menu");
			$("#menu_id").val("");
			$("#title").val("");
			$("#url").val("");
			$("#icon").val("");
			$("#is_active").val("");
		});
	
		$(".menuUbah").on("click", function() {
			$("#modalMenu").html("Edit Menu");
			$(".modal-footer  button[type=submit]").html("Update Menu");
			$(".custom-modal-text form").attr("action", "menu/update");
	
			const id = $(this).data("id");
	
			$.ajax({
				url: "menu/edit",
				data: { id: id },
				method: "POST",
				dataType: "json",
				success: function(data) {
					$("#menu_id").val(data.menu_id);
					$("#title").val(data.title);
					$("#url").val(data.url);
					$("#icon").val(data.icon);					
					$("#is_active").val(data.is_active);
					$("#id").val(data.id);
					// console.log(data);
				}
			});
		});
	});
