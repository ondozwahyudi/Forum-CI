<?php defined('BASEPATH') or exit('No direct script access allowed');
class Login_model extends CI_Model
{
	function validate($email, $password)
	{
		$this->db->where('user_email', $email);
		$this->db->where('user_password', $password);
		$result = $this->db->get('tbl_users');
		return $result;
	}

	function login($usercode, $date)
	{
		$data = array('usercode' => $usercode, 'logindatetime' => $date, 'logoutdatetime' => 0);
		if ($this->db->insert('tblog', $data)) {
			return true;
		} else {
			return false;
		}
	}

	function updatelogin($usercode, $date)
	{
		$data = array('logoutdatetime' => $date);
		$this->db->where('logid', $usercode);
		if ($this->db->update('tblog', $data)) {
			return true;
		} else {
			return false;
		}
	}

	function gettblog()
	{
		$this->db->order_by('logid', 'DESC');
		$query = $this->db->get('tblog')->row_array();
		return $query;
	}
	function register()
	{
		$post = $this->input->post();

		$data = array(
			"name" 	      => $post['name'],
			'user_email' 	 => $post["user_email"],
			'user_password' => md5($post["user_password"]),
			'role_id'	      => 2,
			'is_active'     => 1,
		);
		// $this->db->set('user_uuid', 'UUID()', FALSE);
		$this->db->insert('tbl_users', $data);
	}
}
