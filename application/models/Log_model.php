<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Log_model extends CI_Model
{
    private $_table = "tblog";

    public $logid;
    
    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }
    
    public function getById($logid)
    {
        return $this->db->get_where($this->_table, ["tblog" => $logid])->row();
    }
    
}